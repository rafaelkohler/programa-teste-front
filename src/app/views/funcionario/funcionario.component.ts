import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from  '@angular/router'
import { FuncionarioService } from 'src/app/components/funcionario/funcionario.service';

@Component({
  selector: 'app-funcionario',
  templateUrl: './funcionario.component.html',
  styleUrls: ['./funcionario.component.css']
})
export class FuncionarioComponent implements OnInit {

  pesquisarNome: FormGroup;
  pesquisarCPF: FormGroup

  constructor(private router: Router,
    private fb: FormBuilder,
    private funcionarioService: FuncionarioService) { }

  ngOnInit(): void {
    this.pesquisarNome = this.fb.group({
      nomePesquisa: ['']
    });
    this.pesquisarCPF = this.fb.group({
      cpfPesquisa: ['']
    })
  }

  navigateToFuncionarioCadastrar(): void {
    this.router.navigate(['/funcionarios/cadastrar'])
  }

  pesquisarPorNome() {
    if(this.pesquisarNome.value.nomePesquisa.length > 0) {
      this.funcionarioService.getFuncionarioByNome(this.pesquisarNome.value.nomePesquisa).subscribe(funcionario => {
        this.router.navigate(['/funcionarios/pesquisa', funcionario.id])
      })
    }
  }

  pesquisarPorCPF() {
    if(this.pesquisarCPF.value.cpfPesquisa.length > 0) {
      this.funcionarioService.getFuncionarioByCpf(this.pesquisarCPF.value.cpfPesquisa).subscribe(funcionario => {
        this.router.navigate(['/funcionarios/pesquisa', funcionario.id])
      })
    }
  }

}
