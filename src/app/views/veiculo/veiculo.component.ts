import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { VeiculoService } from 'src/app/components/veiculo/veiculo.service';

@Component({
  selector: 'app-veiculo',
  templateUrl: './veiculo.component.html',
  styleUrls: ['./veiculo.component.css']
})
export class VeiculoComponent implements OnInit {

  pesquisarPlaca: FormGroup;
  pesquisarModelo: FormGroup

  constructor(private router: Router,
    private fb: FormBuilder,
    private veiculoService: VeiculoService) { }


  ngOnInit(): void {
    this.pesquisarPlaca = this.fb.group({
      nomePlaca: ['']
    });
    this.pesquisarModelo = this.fb.group({
      nomeModelo: ['']
    })
  }

  navigateToFuncionarioCadastrar(): void {
    this.router.navigate(['/veiculos/cadastrar'])
  }

  pesquisarPorPlaca() {
    if(this.pesquisarPlaca.value.nomePlaca.length > 0) {
      this.veiculoService.getVeiculoByPlaca(this.pesquisarPlaca.value.nomePlaca).subscribe(veiculo => {
        this.router.navigate(['/veiculos/pesquisa', veiculo.id])
      })
    }
  }

  pesquisarPorModelo() {
    if(this.pesquisarModelo.value.nomeModelo.length > 0) {
      this.veiculoService.getVeiculoByModelo(this.pesquisarModelo.value.nomeModelo).subscribe(veiculo => {
        this.router.navigate(['/veiculos/pesquisa', veiculo.id])
      })
    }
  }

}
