import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { FuncionarioComponent } from './views/funcionario/funcionario.component';
import { VeiculoComponent } from './views/veiculo/veiculo.component';
import { FuncionarioCadastrarComponent } from './components/funcionario/funcionario-cadastrar/funcionario-cadastrar.component';
import { FuncionarioRelatorioComponent } from './components/funcionario/funcionario-relatorio/funcionario-relatorio.component';
import { VeiculoCadastrarComponent } from './components/veiculo/veiculo-cadastrar/veiculo-cadastrar.component';
import { VeiculoRelatorioComponent } from './components/veiculo/veiculo-relatorio/veiculo-relatorio.component';
import { FuncionarioUpdateComponent } from './components/funcionario/funcionario-update/funcionario-update.component';
import { VeiculoUpdateComponent } from './components/veiculo/veiculo-update/veiculo-update.component';
import { FuncionarioPesquisaComponent } from './components/funcionario/funcionario-pesquisa/funcionario-pesquisa.component';
import { VeiculoPesquisaComponent } from './components/veiculo/veiculo-pesquisa/veiculo-pesquisa.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'funcionarios', component: FuncionarioComponent },
  { path: 'funcionarios/cadastrar', component: FuncionarioCadastrarComponent },
  { path: 'funcionarios/atualizar/:id', component: FuncionarioUpdateComponent },
  { path: 'funcionarios/pesquisa/:id', component: FuncionarioPesquisaComponent },
  { path: 'funcionarios/relatorio', component: FuncionarioRelatorioComponent },
  { path: 'veiculos', component: VeiculoComponent },
  { path: 'veiculos/cadastrar', component: VeiculoCadastrarComponent },
  { path: 'veiculos/atualizar/:id', component: VeiculoUpdateComponent },
  { path: 'veiculos/pesquisa/:id', component: VeiculoPesquisaComponent },
  { path: 'veiculos/relatorio', component: VeiculoRelatorioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
