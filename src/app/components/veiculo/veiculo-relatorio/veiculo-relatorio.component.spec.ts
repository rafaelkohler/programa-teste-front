import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeiculoRelatorioComponent } from './veiculo-relatorio.component';

describe('VeiculoRelatorioComponent', () => {
  let component: VeiculoRelatorioComponent;
  let fixture: ComponentFixture<VeiculoRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculoRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculoRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
