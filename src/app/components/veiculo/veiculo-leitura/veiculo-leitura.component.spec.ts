import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeiculoLeituraComponent } from './veiculo-leitura.component';

describe('VeiculoLeituraComponent', () => {
  let component: VeiculoLeituraComponent;
  let fixture: ComponentFixture<VeiculoLeituraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculoLeituraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculoLeituraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
