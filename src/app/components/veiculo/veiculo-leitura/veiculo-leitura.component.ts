import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Veiculo } from '../veiculo.model';
import { VeiculoService } from '../veiculo.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-veiculo-leitura',
  templateUrl: './veiculo-leitura.component.html',
  styleUrls: ['./veiculo-leitura.component.css']
})
export class VeiculoLeituraComponent implements OnInit {

  veiculos: Veiculo[];
  displayedColumns = ['placa', 'modelo', 'anomodelo', 'anofabricacao', 'cor', 'consumomedio', 'quantidadepassageiros', 'chassi', 'datacadastro', 'datadesativacao', 'action']
  
  constructor(private veiculoService: VeiculoService,
    private router: Router) { }

  ngOnInit() {
    this.carregarVeiculos();
  }

  carregarVeiculos() {
    const format = 'dd/MM/yyyy';
    const locale = 'en-US';
    this.veiculoService.getVeiculos().subscribe(veiculos => {
      veiculos.forEach(vei => {
        vei.dataCadastro = formatDate(vei.dataCadastro, format, locale)
        vei.dataDesativacao = formatDate(vei.dataDesativacao, format, locale)
      })
      this.veiculos = veiculos;
    });
  }

  excluir(id): void {
    this.veiculoService.deleteVeiculo(id).subscribe(() => {
      this.veiculoService.showMessage("Cadastro excluido");
      this.carregarVeiculos();
    })
  }

}
