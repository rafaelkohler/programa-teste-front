import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeiculoCadastrarComponent } from './veiculo-cadastrar.component';

describe('VeiculoCadastrarComponent', () => {
  let component: VeiculoCadastrarComponent;
  let fixture: ComponentFixture<VeiculoCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculoCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculoCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
