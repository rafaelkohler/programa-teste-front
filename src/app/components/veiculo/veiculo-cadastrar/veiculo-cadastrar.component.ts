import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Funcionario } from '../../funcionario/funcionario.model';
import { FuncionarioService } from '../../funcionario/funcionario.service';
import { Veiculo, VeiculoAtivo } from '../veiculo.model';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-veiculo-cadastrar',
  templateUrl: './veiculo-cadastrar.component.html',
  styleUrls: ['./veiculo-cadastrar.component.css']
})
export class VeiculoCadastrarComponent implements OnInit {

  veiculo: FormGroup;
  tiposAtivo: Array<VeiculoAtivo> = [];
  constructor(private veiculoService: VeiculoService, 
    private router: Router,
    private fb: FormBuilder) { }


  ngOnInit(): void {
    this.veiculo = this.fb.group({
      placa: ['', Validators.required],
      ativo: ['', Validators.required],
      anoFabricacao: ['', Validators.required],
      anoModelo: ['', Validators.required],
      chassi: ['', Validators.required],
      modelo: ['', Validators.required],
      cor: ['', Validators.required],
      consumoMedioPorKm: ['', Validators.required],
      quantidadePassageiro: ['', Validators.required],
      dataCadastro: ['', Validators.required],
      dataDesativacao: ['', Validators.required]
    });
    this.tiposAtivo = this.veiculoService.getAtivoOpitions();
  }

  createVeiculo() {
    this.veiculo.value.dataCadastro = new Date(this.veiculo.value.dataCadastro).toJSON()
    this.veiculo.value.dataDesativacao = new Date(this.veiculo.value.dataDesativacao).toJSON()
    this.veiculo.value.anoFabricacao = this.veiculo.value.anoFabricacao.toString()
    this.veiculo.value.anoModelo = this.veiculo.value.anoModelo.toString()
    this.veiculo.value.consumoMedioPorKm = this.veiculo.value.consumoMedioPorKm.toString()
    this.veiculo.value.quantidadePassageiro = this.veiculo.value.quantidadePassageiro.toString()
    console.log(this.veiculo.value )
    this.veiculoService.createVeiculo(this.veiculo.value).subscribe(() => {
      this.veiculoService.showMessage('Veículo cadastrado.');
      this.router.navigate(['/veiculos']);
    }, error => {
      console.error(error);
    });
  }

  cancel(): void {
    this.router.navigate(['/veiculos']);
  }

}
