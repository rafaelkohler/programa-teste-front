interface Veiculo {
    id?: number;
    placa: string;
    ativo: string;
    anoFabricacao: number;
    anoModelo: number;
    chassi: string;
    dataCadastro: string;
    dataDesativacao: string;
    modelo: string;
    cor: string;
    consumoMedioPorKm: number;
    quantidadePassageiro: number;
}

interface VeiculoAtivo {
    tipo: string
}

export {Veiculo, VeiculoAtivo};