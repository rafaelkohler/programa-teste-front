import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { API_CONFIG } from 'src/app/config/api.config';
import { Veiculo, VeiculoAtivo } from './veiculo.model';

@Injectable({
  providedIn: 'root'
})
export class VeiculoService {
  baseUrl = `${API_CONFIG.baseUrl}/veiculos`

  constructor(private http: HttpClient,
    private snackBar: MatSnackBar) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, '', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  getVeiculos(): Observable<Veiculo[]> {
    return this.http.get<Veiculo[]>(this.baseUrl);
  }

  getVeiculoById(id: number): Observable<Veiculo> {
    return this.http.get<Veiculo>(`${this.baseUrl}/${id}`);
  }

  getVeiculoByModelo(modelo: string): Observable<Veiculo> {
    return this.http.get<Veiculo>(`${this.baseUrl}/findbymodelo/${modelo}`);
  }

  getVeiculoByPlaca(placa: string): Observable<Veiculo> {
    return this.http.get<Veiculo>(`${this.baseUrl}/findbyplaca/${placa}`);
  }

  createVeiculo(veiculo: Veiculo): Observable<Veiculo> {
    return this.http.post<Veiculo>(this.baseUrl, veiculo);
  }

  updateVeiculo(veiculo: Veiculo): Observable<Veiculo> {
    return this.http.put<Veiculo>(`${this.baseUrl}/${veiculo.id}`, veiculo);
  }

  deleteVeiculo(id: number): Observable<Veiculo> {
    return this.http.delete<Veiculo>(`${this.baseUrl}/${id}`);
  }

  getAtivoOpitions(): Array<VeiculoAtivo> {
    return [{ tipo: 'Sim' }, { tipo: 'Não' }]
  }
}
