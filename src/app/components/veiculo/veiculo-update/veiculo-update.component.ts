import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Veiculo, VeiculoAtivo } from '../veiculo.model';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-veiculo-update',
  templateUrl: './veiculo-update.component.html',
  styleUrls: ['./veiculo-update.component.css']
})
export class VeiculoUpdateComponent implements OnInit {

  veiculo: FormGroup;
  tiposAtivo: Array<VeiculoAtivo> = [];

  constructor(private veiculoService: VeiculoService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.veiculo = this.fb.group({
      placa: ['', Validators.required],
      ativo: ['', Validators.required],
      anoFabricacao: ['', Validators.required],
      anoModelo: ['', Validators.required],
      chassi: ['', Validators.required],
      modelo: ['', Validators.required],
      cor: ['', Validators.required],
      consumoMedioPorKm: ['', Validators.required],
      quantidadePassageiro: ['', Validators.required],
      dataCadastro: ['', Validators.required],
      dataDesativacao: ['', Validators.required]
    });
    this.tiposAtivo = this.veiculoService.getAtivoOpitions();


    const id = this.route.snapshot.paramMap.get('id');
    this.veiculoService.getVeiculoById(Number(id)).subscribe(veiculo => {
      this.veiculo.controls['placa'].setValue(veiculo.placa);
      this.veiculo.controls['ativo'].setValue(veiculo.ativo);
      this.veiculo.controls['anoFabricacao'].setValue(veiculo.anoFabricacao);
      this.veiculo.controls['anoModelo'].setValue(veiculo.anoModelo);
      this.veiculo.controls['chassi'].setValue(veiculo.chassi);
      this.veiculo.controls['modelo'].setValue(veiculo.modelo);
      this.veiculo.controls['cor'].setValue(veiculo.cor);
      this.veiculo.controls['consumoMedioPorKm'].setValue(veiculo.consumoMedioPorKm);
      this.veiculo.controls['quantidadePassageiro'].setValue(veiculo.quantidadePassageiro);
      this.veiculo.controls['dataCadastro'].setValue(veiculo.dataCadastro);
      this.veiculo.controls['dataDesativacao'].setValue(veiculo.dataDesativacao);
    })

  }

  updateVeiculo(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.veiculo.value.id = id;
    this.veiculo.value.dataCadastro = new Date(this.veiculo.value.dataCadastro).toJSON()
    this.veiculo.value.dataDesativacao = new Date(this.veiculo.value.dataDesativacao).toJSON()
    this.veiculo.value.anoFabricacao = this.veiculo.value.anoFabricacao.toString()
    this.veiculo.value.anoModelo = this.veiculo.value.anoModelo.toString()
    this.veiculo.value.consumoMedioPorKm = this.veiculo.value.consumoMedioPorKm.toString()
    this.veiculo.value.quantidadePassageiro = this.veiculo.value.quantidadePassageiro.toString()
    this.veiculoService.updateVeiculo(this.veiculo.value).subscribe(() => {
      this.veiculoService.showMessage('Cadastro atualizado.')
      this.router.navigate(['/veiculos']);
    })
  }

  cancel(): void {
    this.router.navigate(['/veiculos']);
  }

}
