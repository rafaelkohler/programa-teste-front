import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Veiculo } from '../veiculo.model';
import { VeiculoService } from '../veiculo.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-veiculo-pesquisa',
  templateUrl: './veiculo-pesquisa.component.html',
  styleUrls: ['./veiculo-pesquisa.component.css']
})
export class VeiculoPesquisaComponent implements OnInit {

  veiculo: Veiculo

  constructor(private veiculoService: VeiculoService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const format = 'dd/MM/yyyy';
    const locale = 'en-US';
    const id = this.route.snapshot.paramMap.get('id');
    this.veiculoService.getVeiculoById(Number(id)).subscribe(veiculo  => {
      veiculo: {
        veiculo.dataCadastro = formatDate(veiculo.dataCadastro, format, locale)
        veiculo.dataDesativacao = formatDate(veiculo.dataDesativacao, format, locale)
      }
      this.veiculo = veiculo
    })
  }

}
