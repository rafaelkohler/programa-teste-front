export interface Funcionario {
    id?: number;
    cpf: number;
    nome: string;
    dataNascimento: any;
    login: string;
    senha: string;
}