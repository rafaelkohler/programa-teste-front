import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Funcionario } from '../funcionario.model';
import { FuncionarioService } from '../funcionario.service';

@Component({
  selector: 'app-funcionario-update',
  templateUrl: './funcionario-update.component.html',
  styleUrls: ['./funcionario-update.component.css']
})
export class FuncionarioUpdateComponent implements OnInit {
  funcionario: Funcionario;
  dataForm: '';
  constructor(private funcionarioService: FuncionarioService, 
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const id =  this.route.snapshot.paramMap.get('id');
    this.funcionarioService.getFuncionarioById(Number(id)).subscribe(funcionario  => {
      this.funcionario = funcionario;
      this.dataForm = funcionario.dataNascimento;
    })
  }

  updateFuncionario(): void {
    const data = new Date(this.dataForm).toLocaleString()
    let x = data.split(' ');
    if(this.funcionario.dataNascimento != x[0]) {
      this.funcionario.dataNascimento = data;
    }
    this.funcionarioService.updateFuncionario(this.funcionario).subscribe(() => {
      this.funcionarioService.showMessage('Cadastro atualizado.')
      this.router.navigate(['/funcionarios']);
    })
  }

  cancel(): void {
    this.router.navigate(['/funcionarios']);
  }

}
