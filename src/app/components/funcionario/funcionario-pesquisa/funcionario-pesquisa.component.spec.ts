import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionarioPesquisaComponent } from './funcionario-pesquisa.component';

describe('FuncionarioPesquisaComponent', () => {
  let component: FuncionarioPesquisaComponent;
  let fixture: ComponentFixture<FuncionarioPesquisaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionarioPesquisaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionarioPesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
