import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FuncionarioService } from '../funcionario.service';
import { Funcionario } from '../funcionario.model';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-funcionario-pesquisa',
  templateUrl: './funcionario-pesquisa.component.html',
  styleUrls: ['./funcionario-pesquisa.component.css']
})
export class FuncionarioPesquisaComponent implements OnInit {
  
  funcionario: Funcionario;

  constructor(private funcionarioService: FuncionarioService, 
    private route: ActivatedRoute) { }


  ngOnInit() {
    const format = 'dd/MM/yyyy';
    const locale = 'en-US';
    const id = this.route.snapshot.paramMap.get('id');
    this.funcionarioService.getFuncionarioById(Number(id)).subscribe(funcionario  => {
      funcionario: {
        funcionario.dataNascimento = formatDate(funcionario.dataNascimento, format, locale)
      }
      this.funcionario = funcionario
    })
  }

}
