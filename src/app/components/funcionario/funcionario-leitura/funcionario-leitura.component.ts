import { Component, OnInit } from '@angular/core';
import { Funcionario } from '../funcionario.model';
import { FuncionarioService } from '../funcionario.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-funcionario-leitura',
  templateUrl: './funcionario-leitura.component.html',
  styleUrls: ['./funcionario-leitura.component.css']
})
export class FuncionarioLeituraComponent implements OnInit {

  funcionarios: Funcionario[]
  displayedColumns = ['nome', 'cpf', 'dataNascimento', 'login', 'senha', 'action']

  constructor(private funcionarioService: FuncionarioService) { }

  ngOnInit(): void {
    this.carregarFuncionarios();
  }

  carregarFuncionarios() {
    const format = 'dd/MM/yyyy';
    const locale = 'en-US';
    this.funcionarioService.getFuncionarios().subscribe(funcionarios => {
      funcionarios.forEach(fun => {
        fun.dataNascimento = formatDate(fun.dataNascimento, format, locale)
      })
      this.funcionarios = funcionarios
    });
  }

  excluir(id): void {
    this.funcionarioService.deleteFuncionario(id).subscribe(() => {
      this.funcionarioService.showMessage("Cadastro excluido");
      this.carregarFuncionarios();
    })
  }


}
