import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuncionarioLeituraComponent } from './funcionario-leitura.component';

describe('FuncionarioLeituraComponent', () => {
  let component: FuncionarioLeituraComponent;
  let fixture: ComponentFixture<FuncionarioLeituraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuncionarioLeituraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuncionarioLeituraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
