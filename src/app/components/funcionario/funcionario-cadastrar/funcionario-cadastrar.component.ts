import { Component, OnInit } from '@angular/core';
import { FuncionarioService } from '../funcionario.service';
import { Router } from '@angular/router'
import { Funcionario } from '../funcionario.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-funcionario-cadastrar',
  templateUrl: './funcionario-cadastrar.component.html',
  styleUrls: ['./funcionario-cadastrar.component.css']
})
export class FuncionarioCadastrarComponent implements OnInit {

  funcionario: FormGroup;

  constructor(private funcionarioService: FuncionarioService, 
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.funcionario= this.fb.group({
      cpf: ['', Validators.required],
      nome: ['', Validators.required],
      dataNascimento: ['', Validators.required],
      login: ['', Validators.required],
      senha: ['', Validators.required],
    })
  }

  createFuncionario(): void {
    console.log(new Date(this.funcionario.value.dataNascimento).toJSON())
    this.funcionario.value.dataNascimento = new Date(this.funcionario.value.dataNascimento).toJSON()
    this.funcionarioService.createFuncionario(this.funcionario.value).subscribe(() => {
      this.funcionarioService.showMessage('Funcionário cadastrado.');
      this.router.navigate(['/funcionarios']);
    });
  }

  cancel(): void {
    this.router.navigate(['/funcionarios']);
  }

}
