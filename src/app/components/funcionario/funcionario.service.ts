import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_CONFIG } from 'src/app/config/api.config';
import { Funcionario } from './funcionario.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class FuncionarioService {

  baseUrl = `${API_CONFIG.baseUrl}/funcionarios`

  constructor(private http: HttpClient, 
    private snackBar: MatSnackBar) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, '', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  getFuncionarios(): Observable<Funcionario[]> {
    return this.http.get<Funcionario[]>(this.baseUrl);
  }

  getFuncionarioById(id: number): Observable<Funcionario> {
    return this.http.get<Funcionario>(`${this.baseUrl}/${id}`);
  }

  getFuncionarioByNome(nome: string): Observable<Funcionario> {
    return this.http.get<Funcionario>(`${this.baseUrl}/findbynome/${nome}`);
  }

  getFuncionarioByCpf(cpf: string): Observable<Funcionario> {
    return this.http.get<Funcionario>(`${this.baseUrl}/findbycpf/${cpf}`);
  }

  createFuncionario(funcionario: Funcionario): Observable<Funcionario> {
    return this.http.post<Funcionario>(this.baseUrl, funcionario);
  }

  updateFuncionario(funcionario: Funcionario): Observable<Funcionario> {
    return this.http.put<Funcionario>(`${this.baseUrl}/${funcionario.id}`, funcionario);
  }

  deleteFuncionario(id: number): Observable<Funcionario> {
    return this.http.delete<Funcionario>(`${this.baseUrl}/${id}`);
  }
}
