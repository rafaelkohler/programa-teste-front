import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { HeaderComponent } from './components/template/header/header.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { NavComponent } from './components/template/nav/nav.component'
import { HomeComponent } from './views/home/home.component'
import { FuncionarioComponent } from './views/funcionario/funcionario.component';
import { FuncionarioCadastrarComponent } from './components/funcionario/funcionario-cadastrar/funcionario-cadastrar.component';
import { FuncionarioRelatorioComponent } from './components/funcionario/funcionario-relatorio/funcionario-relatorio.component';
import { VeiculoComponent } from './views/veiculo/veiculo.component';
import { VeiculoCadastrarComponent } from './components/veiculo/veiculo-cadastrar/veiculo-cadastrar.component';
import { VeiculoRelatorioComponent } from './components/veiculo/veiculo-relatorio/veiculo-relatorio.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatCardModule } from '@angular/material/card'
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from "@angular/material/select"
import { MatDatepickerModule } from '@angular/material/datepicker'  
import { MatNativeDateModule } from '@angular/material/core';
import { FuncionarioLeituraComponent } from './components/funcionario/funcionario-leitura/funcionario-leitura.component';
import { MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { FuncionarioUpdateComponent } from './components/funcionario/funcionario-update/funcionario-update.component';
import { VeiculoLeituraComponent } from './components/veiculo/veiculo-leitura/veiculo-leitura.component';
import { VeiculoUpdateComponent } from './components/veiculo/veiculo-update/veiculo-update.component';
import { FuncionarioPesquisaComponent } from './components/funcionario/funcionario-pesquisa/funcionario-pesquisa.component';
import { VeiculoPesquisaComponent } from './components/veiculo/veiculo-pesquisa/veiculo-pesquisa.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    HomeComponent,
    FuncionarioComponent,
    VeiculoComponent,
    FuncionarioCadastrarComponent,
    VeiculoCadastrarComponent,
    VeiculoRelatorioComponent,
    FuncionarioRelatorioComponent,
    FuncionarioLeituraComponent,
    FuncionarioUpdateComponent,
    VeiculoLeituraComponent,
    VeiculoUpdateComponent,
    FuncionarioPesquisaComponent,
    VeiculoPesquisaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
